var express = require('express');
var router = express.Router();
var datos = require('../controllers/datosController');

// Rutas de los controladores
router.post('/mostrarAlumnos', datos.mostrarAlumnos);
router.post('/createAlumnos', datos.createAlumnos);
router.post('/readAlumnos', datos.readAlumnos);
router.post('/updateAlumnos', datos.updateAlumnos);
router.post('/deleteAlumnos', datos.deleteAlumnos);

module.exports = router