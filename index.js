const express = require( 'express');
const app = express()
var bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
es6Renderer = require('express-es6-template-engine'),
app.engine('html', es6Renderer);
app.set('views', 'views');
app.set('view engine', 'html')

//Alumnos
var datos = require('./routes/datosRoutes')  
app.use('/datos', datos);

app.listen (3000, ()=>{
console. log('Listening at localhost: 3000')
})