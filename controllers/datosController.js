const { connection } = require('../database/connect');

// Mostrar todos los alumnos
const mostrarAlumnos = async  (req,res) =>{
  const query = `SELECT * FROM ALUMNOS`
  connection.query(
    query,
    function (err, results, fields) {
      console.log(err);                     
      res.json(results); 
    }
  );
}

// Crear un alumno
const createAlumnos = (req,res) =>{
  const {nombre, apellido_materno, apellido_paterno, direccion} = req.body
  const query = `INSERT into ALUMNOS ( nombre, apellido_materno, apellido_paterno, direccion ) values (?,?,?,?)`
  connection.query(
    query,
    [nombre, apellido_materno, apellido_paterno, direccion],
    function (err, results, fields) {
      if(err) {
        res.json({error: err})
      }else{
        res.json({message: 'Alumno creado'})
      } 
    }
  )
}

// Buscar un alumno
const readAlumnos = (req,res) =>{
  const {busqueda} = req.body
  const query = `SELECT *
  FROM ALUMNOS
  WHERE nombre LIKE CONCAT('%', ?, '%')
  OR apellido_materno LIKE CONCAT('%', ?, '%')
  OR apellido_paterno LIKE CONCAT('%', ?, '%')
  OR direccion LIKE CONCAT('%', ?, '%')`
  connection.query(
    query,
    [busqueda, busqueda, busqueda, busqueda],
    function (err, results, fields) {
      if(err) {
        res.json({error: err})
      }else{
        res.json(results)
      } 
    }
  )
}

// Actualizar datos de un alumno
const updateAlumnos = (req,res) =>{
  const {nombre, apellido_materno, apellido_paterno, direccion,id} = req.body
  const query = `UPDATE ALUMNOS set nombre = ? , apellido_materno = ?, apellido_paterno = ?, direccion = ? where id = ?`
  connection.query(
    query,
    [nombre, apellido_materno, apellido_paterno, direccion, id],
     function (err, results, fields) {
      if(err) {
        res.json({error: err})
      }else{
        if(results.affectedRows > 0){
          res.json({message: 'Alumno actualizado'})
        }else{
          res.json({message: 'Alumno no encontrado'})
        }
      } 
    }
  )
}

// Eliminar un alumno, mediante numero de ID
const deleteAlumnos = (req,res) =>{
  const {id} = req.body
  const query = `DELETE from ALUMNOS where id = ?`
  connection.query(
    query,
    [id],
    function (err, results, fields) {
      if(err) {
        res.json({error: err})
      }else{
        if(results.affectedRows > 0){
          res.json({message: 'Alumno eliminado'})
        }else{
          res.json({message: 'Alumno no encontrado'})
        }
      } 
    }
  )
}

module.exports = {
  mostrarAlumnos,
  createAlumnos,
  readAlumnos,
  updateAlumnos,
  deleteAlumnos
}